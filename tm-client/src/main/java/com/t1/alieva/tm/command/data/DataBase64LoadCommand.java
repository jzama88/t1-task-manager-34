package com.t1.alieva.tm.command.data;

import com.t1.alieva.tm.dto.request.data.DataBase64LoadRequest;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Load data from BASE64 file";

    @NotNull
    public static final String NAME = "data-load-base64";

    @SneakyThrows
    @Override
    public void execute()
    {
        System.out.println("[DATA BASE64 LOAD]");
        serviceLocator.getDomainEndpoint().loadDataBase64(new DataBase64LoadRequest(getToken()));
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }
}
