package com.t1.alieva.tm.command.data;

import com.t1.alieva.tm.dto.request.data.DataJsonSaveFasterXmlRequest;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataJsonSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Save data in json file";

    @NotNull
    public static final String NAME = "data-save-json-faster";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE JSON]");
        serviceLocator.getDomainEndpoint().saveDataJsonFasterXml(new DataJsonSaveFasterXmlRequest(getToken()));
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }
}
