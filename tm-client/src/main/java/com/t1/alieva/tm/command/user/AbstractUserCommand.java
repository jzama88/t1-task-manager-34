package com.t1.alieva.tm.command.user;


import com.t1.alieva.tm.api.endpoint.IAuthEndpoint;
import com.t1.alieva.tm.api.endpoint.IUserEndpoint;
import com.t1.alieva.tm.command.AbstractCommand;
import com.t1.alieva.tm.exception.entity.UserNotFoundException;
import com.t1.alieva.tm.model.User;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractUserCommand extends AbstractCommand {

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @NotNull
    public IUserEndpoint getUserEndpoint() {
        return serviceLocator.getUserEndpoint();
    }

    @NotNull
    public IAuthEndpoint getAuthEndpoint() {
        return serviceLocator.getAuthEndpoint();
    }

    @SneakyThrows
    protected void showUser(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }
}
