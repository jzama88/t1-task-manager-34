package com.t1.alieva.tm.api.service;

import com.t1.alieva.tm.api.endpoint.*;
import org.jetbrains.annotations.NotNull;

public interface IServiceLocator {

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IAuthEndpoint getAuthEndpoint();

    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    ISystemEndpoint getSystemEndpoint();

    @NotNull
    IDomainEndpoint getDomainEndpoint();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

    @NotNull
    IUserEndpoint getUserEndpoint();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ITokenService getTokenService();
}
