package com.t1.alieva.tm.api.model;

import com.t1.alieva.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.naming.AuthenticationException;
import java.io.IOException;


public interface ICommand {

    //String toString();

    @NotNull
    String getName();

    @Nullable
    String getArgument();

    @NotNull
    String getDescription();

    void execute() throws AuthenticationException, IOException, ClassNotFoundException, AbstractException;
}
