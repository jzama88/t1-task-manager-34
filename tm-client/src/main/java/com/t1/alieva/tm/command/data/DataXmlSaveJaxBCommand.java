package com.t1.alieva.tm.command.data;

import com.t1.alieva.tm.dto.request.data.DataXmlSaveJaxBRequest;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataXmlSaveJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Save data in XML file";

    @NotNull
    public static final String NAME = "data-save-xml-jaxb";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE XML]");
        serviceLocator.getDomainEndpoint().saveDataXmlJaxB(new DataXmlSaveJaxBRequest(getToken()));
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }
}
