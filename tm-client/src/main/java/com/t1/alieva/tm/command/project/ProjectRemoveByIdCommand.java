package com.t1.alieva.tm.command.project;

import com.t1.alieva.tm.dto.request.project.ProjectRemoveByIdRequest;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.system.AbstractSystemException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getName() {
        return "p-remove-by-id";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Remove Project by ID.";
    }

    @Override
    public void execute() throws
            AbstractEntityNotFoundException,
            AbstractSystemException,
            AbstractFieldException,
            AbstractUserException
    {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(getToken());
        request.setId(id);
        getProjectEndpoint().removeProjectById(request);
    }
}
