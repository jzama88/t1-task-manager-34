package com.t1.alieva.tm.command.task;

import com.t1.alieva.tm.dto.request.task.TaskUnbindFromProjectRequest;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.system.AbstractSystemException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getName() {
        return "t-unbind-to-project";
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Task unbind to project.";
    }

    @Override
    public void execute() throws
            AbstractEntityNotFoundException,
            AbstractFieldException,
            AbstractSystemException,
            AbstractUserException {
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.println("[ENTER PROJECT ID]");
        @NotNull final String projectID = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID]");
        @NotNull final String taskID = TerminalUtil.nextLine();
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(getToken());
        request.setProjectId(projectID);
        request.setTaskId(taskID);
        getTaskEndpoint().unbindTaskFromProject(request);
    }
}
