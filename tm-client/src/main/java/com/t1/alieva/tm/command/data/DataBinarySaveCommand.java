package com.t1.alieva.tm.command.data;

import com.t1.alieva.tm.dto.request.data.DataBinarySaveRequest;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataBinarySaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Save data to binary file";

    @NotNull
    public static final String NAME = "data-save-bin";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE BINARY]");
        serviceLocator.getDomainEndpoint().saveDataBinary(new DataBinarySaveRequest(getToken()));
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }
}
