package com.t1.alieva.tm.command.user;

import com.t1.alieva.tm.dto.request.user.UserLockRequest;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.system.AbstractSystemException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public class UserLockCommand extends AbstractUserCommand {

    @Override
    public void execute() throws
            AbstractEntityNotFoundException,
            AbstractFieldException,
            AbstractSystemException,
            AbstractUserException {
        System.out.println("[USER LOCK]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserLockRequest request = new UserLockRequest(getToken());
        request.setLogin(login);
        getUserEndpoint().lockUser(request);
    }

    @Override
    @NotNull
    public String getDescription() {
        return "user lock";
    }

    @Override
    @NotNull
    public String getName() {
        return "user-lock";
    }
}
