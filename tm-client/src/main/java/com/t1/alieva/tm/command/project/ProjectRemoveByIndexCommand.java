package com.t1.alieva.tm.command.project;

import com.t1.alieva.tm.dto.request.project.ProjectRemoveByIndexRequest;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.system.AbstractSystemException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getName() {
        return "p-remove-by-index";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Remove Project by Index.";
    }

    @Override
    public void execute() throws
            AbstractEntityNotFoundException,
            AbstractSystemException,
            AbstractFieldException,
            AbstractUserException
    {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(getToken());
        request.setIndex(index);
        getProjectEndpoint().removeProjectByIndex(request);
    }
}
