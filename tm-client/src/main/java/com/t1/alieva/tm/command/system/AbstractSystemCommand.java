package com.t1.alieva.tm.command.system;

import com.t1.alieva.tm.api.service.ICommandService;
import com.t1.alieva.tm.api.service.IPropertyService;
import com.t1.alieva.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    protected ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

    @NotNull
    protected IPropertyService getPropertyService() {
        return getServiceLocator().getPropertyService();
    }
}
