package com.t1.alieva.tm.command.data;

import com.t1.alieva.tm.dto.request.data.DataBinaryLoadRequest;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataBinaryLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Load data from binary file";

    @NotNull
    public static final String NAME = "data-load-bin";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA BINARY LOAD]");
        serviceLocator.getDomainEndpoint().loadDataBinary(new DataBinaryLoadRequest(getToken()));
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }
}
