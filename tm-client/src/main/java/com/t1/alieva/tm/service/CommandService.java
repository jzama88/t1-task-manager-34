package com.t1.alieva.tm.service;

import com.t1.alieva.tm.api.repository.ICommandRepository;
import com.t1.alieva.tm.api.service.ICommandService;
import com.t1.alieva.tm.command.AbstractCommand;
import com.t1.alieva.tm.exception.system.ArgumentNotSupportedException;
import com.t1.alieva.tm.exception.system.CommandNotSupportedException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public void add(@Nullable AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

    @Nullable
    public AbstractCommand getCommandByName(@Nullable final String name) throws CommandNotSupportedException {
        if (name == null || name.isEmpty()) throw new CommandNotSupportedException();
        @NotNull AbstractCommand command = commandRepository.getCommandByName(name);
        if (command == null) throw new CommandNotSupportedException();
        return command;

    }

    @Nullable
    public AbstractCommand getCommandByArgument(@Nullable final String argument) throws ArgumentNotSupportedException {
        if (argument == null || argument.isEmpty()) throw new ArgumentNotSupportedException();
        @NotNull AbstractCommand command = commandRepository.getCommandByArgument(argument);
        if (command == null) throw new ArgumentNotSupportedException();
        return command;

    }

    @Override
    @Nullable
    public Collection<AbstractCommand> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    @Override
    public @NotNull Collection<AbstractCommand> getCommandsWithArgument() {
        return commandRepository.getCommandsWithArgument();
    }
}
