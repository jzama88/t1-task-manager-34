package com.t1.alieva.tm.command.project;

import com.t1.alieva.tm.dto.request.project.ProjectClearRequest;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.system.AbstractSystemException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import org.jetbrains.annotations.NotNull;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getName() {
        return "project-clear";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public void execute() throws
            AbstractEntityNotFoundException,
            AbstractSystemException,
            AbstractFieldException,
            AbstractUserException {
        System.out.println("[CLEAR PROJECT]");
        getProjectEndpoint().clearProject(new ProjectClearRequest(getToken()));
    }
}
