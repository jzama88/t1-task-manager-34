package com.t1.alieva.tm.command.project;

import com.t1.alieva.tm.dto.request.project.ProjectUpdateByIndexRequest;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.system.AbstractSystemException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getName() {
        return "p-update-by-index";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Update Project by Index.";
    }

    @Override
    public void execute() throws
            AbstractEntityNotFoundException,
            AbstractSystemException,
            AbstractFieldException,
            AbstractUserException {
        System.out.println(("[SHOW PROJECT BY INDEX]"));
        System.out.println(("[ENTER INDEX]"));
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println(("[ENTER NAME]"));
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println(("[ENTER DESCRIPTION]"));
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(getToken());
        request.setIndex(index);
        request.setName(name);
        request.setDescription(description);
        getProjectEndpoint().updateProjectByIndex(request);
    }
}
