package com.t1.alieva.tm.api.service;

import com.t1.alieva.tm.api.component.ISaltProvider;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @Nullable
    Integer getServerPort();
}
