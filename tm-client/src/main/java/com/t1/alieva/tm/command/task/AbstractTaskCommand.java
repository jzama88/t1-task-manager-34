package com.t1.alieva.tm.command.task;

import com.t1.alieva.tm.api.endpoint.IAuthEndpoint;
import com.t1.alieva.tm.api.endpoint.ITaskEndpoint;
import com.t1.alieva.tm.command.AbstractCommand;
import com.t1.alieva.tm.enumerated.Status;
import com.t1.alieva.tm.exception.entity.TaskNotFoundException;
import com.t1.alieva.tm.model.Task;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    public ITaskEndpoint getTaskEndpoint() {
        return serviceLocator.getTaskEndpoint();
    }

    @NotNull
    public IAuthEndpoint getAuthEndpoint() {
        return serviceLocator.getAuthEndpoint();
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    protected void renderTasks(@Nullable final List<Task> tasks) {
        if (tasks == null) return;
        int index = 1;
        for (@Nullable final Task task : tasks) {
            if (task == null) continue;
            String print = index++ + ". " +
                    task.getName() + " | " +
                    task.getId() + " | " +
                    task.getStatus();
            System.out.println(print);
        }
    }

    @SneakyThrows
    protected void showTask(@Nullable final Task task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }
}
