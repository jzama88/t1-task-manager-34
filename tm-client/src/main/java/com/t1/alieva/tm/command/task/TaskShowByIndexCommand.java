package com.t1.alieva.tm.command.task;

import com.t1.alieva.tm.dto.request.task.TaskGetByIndexRequest;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.system.AbstractSystemException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.model.Task;
import com.t1.alieva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getName() {
        return "t-show-by-index";
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show Task by Index.";
    }

    @Override
    public void execute() throws
            AbstractEntityNotFoundException,
            AbstractFieldException,
            AbstractSystemException,
            AbstractUserException {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskGetByIndexRequest request = new TaskGetByIndexRequest(getToken());
        request.setIndex(index);
        @Nullable final Task task = getTaskEndpoint().getTaskByIndex(request).getTask();
        showTask(task);
    }
}
