package com.t1.alieva.tm.command.system;

import com.t1.alieva.tm.dto.request.system.ServerVersionRequest;
import com.t1.alieva.tm.dto.response.system.ServerVersionResponse;
import com.t1.alieva.tm.exception.system.AbstractSystemException;
import org.jetbrains.annotations.NotNull;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    private static final String ARGUMENT = "-v";

    @NotNull
    private static final String DESCRIPTION = "Show program version.";

    @NotNull
    private static final String NAME = "version";

    @Override
    public void execute() throws
            AbstractSystemException {
        System.out.println("[VERSION]");
        @NotNull final ServerVersionResponse response = getServiceLocator().getSystemEndpoint().getVersion(new ServerVersionRequest());
        System.out.println(response.getVersion());
    }

    @Override
    public @NotNull String getArgument() {
        return ARGUMENT;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }
}
