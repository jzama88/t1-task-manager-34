package com.t1.alieva.tm.command.project;

import com.t1.alieva.tm.dto.request.project.ProjectGetByIndexRequest;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.system.AbstractSystemException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.model.Project;
import com.t1.alieva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


public final class ProjectShowByIndexCommand extends AbstractProjectCommand {
    @Override
    @NotNull
    public String getName() {
        return "p-start-by-index";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Start Project by Index.";
    }

    @Override
    public void execute() throws
            AbstractEntityNotFoundException,
            AbstractSystemException,
            AbstractFieldException,
            AbstractUserException
    {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectGetByIndexRequest request = new ProjectGetByIndexRequest(getToken());
        request.setIndex(index);
        @Nullable final Project project = getProjectEndpoint().getProjectByIndex(request).getProject();
        showProject(project);
    }
}
