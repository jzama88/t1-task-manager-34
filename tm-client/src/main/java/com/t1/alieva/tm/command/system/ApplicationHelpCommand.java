package com.t1.alieva.tm.command.system;

import com.t1.alieva.tm.api.model.ICommand;
import com.t1.alieva.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;

public final class ApplicationHelpCommand extends AbstractSystemCommand {

    @NotNull
    private static final String ARGUMENT = "-h";

    @NotNull
    private static final String DESCRIPTION = "Show commands list.";

    @NotNull
    private static final String NAME = "help";

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (@NotNull final ICommand command : commands)
            System.out.println(command);
    }

    @Override
    public @NotNull String getArgument() {
        return ARGUMENT;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }
}
