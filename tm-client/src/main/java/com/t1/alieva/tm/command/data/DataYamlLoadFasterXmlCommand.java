package com.t1.alieva.tm.command.data;

import com.t1.alieva.tm.dto.request.data.DataYamlLoadFasterXmlRequest;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataYamlLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Load data from yaml file";

    @NotNull
    public static final String NAME = "data-load-yaml-faster";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD YAML]");
        serviceLocator.getDomainEndpoint().loadDataYamlFasterXml(new DataYamlLoadFasterXmlRequest(getToken()));
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }
}

