package com.t1.alieva.tm.api.repository;

import com.t1.alieva.tm.command.AbstractCommand;
import com.t1.alieva.tm.exception.system.ArgumentNotSupportedException;
import com.t1.alieva.tm.exception.system.CommandNotSupportedException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ICommandRepository {

    void add(@Nullable AbstractCommand command);

    @Nullable
    AbstractCommand getCommandByArgument(@NotNull String argument) throws ArgumentNotSupportedException;

    @Nullable
    AbstractCommand getCommandByName(@NotNull String name) throws CommandNotSupportedException;

    @Nullable
    Collection<AbstractCommand> getTerminalCommands();

    @NotNull
    Collection<AbstractCommand> getCommandsWithArgument();
}
