package com.t1.alieva.tm.service;

import com.t1.alieva.tm.api.service.ITokenService;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class TokenService implements ITokenService {
    @Nullable
    private String token;
}
