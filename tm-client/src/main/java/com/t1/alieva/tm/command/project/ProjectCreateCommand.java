package com.t1.alieva.tm.command.project;

import com.t1.alieva.tm.dto.request.project.ProjectCreateRequest;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getName() {
        return "project-create";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() throws AbstractEntityNotFoundException, AbstractFieldException, AbstractUserException, IOException, ClassNotFoundException {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(getToken());
        request.setName(name);
        request.setDescription(description);
        getProjectEndpoint().createProject(request);
    }
}
