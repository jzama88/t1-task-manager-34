package com.t1.alieva.tm.command.data;

import com.t1.alieva.tm.dto.request.data.DataBase64SaveRequest;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataBase64SaveCommand extends AbstractDataCommand {


    @NotNull
    public static final String DESCRIPTION = "Save data to BASE64 file";

    @NotNull
    public static final String NAME = "data-save-base64";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA BASE64 SAVE]");
        serviceLocator.getDomainEndpoint().saveDataBase64(new DataBase64SaveRequest(getToken()));
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }
}
