package com.t1.alieva.tm.command.data;

import com.t1.alieva.tm.dto.request.data.DataYamlSaveFasterXmlRequest;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Save data in yaml file";

    @NotNull
    public static final String NAME = "data-save-yaml-faster";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE YAML]");
        serviceLocator.getDomainEndpoint().saveDataYamlFasterXml(new DataYamlSaveFasterXmlRequest(getToken()));
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }
}
