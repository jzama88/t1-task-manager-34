package com.t1.alieva.tm.component;

import com.t1.alieva.tm.api.endpoint.*;
import com.t1.alieva.tm.api.repository.ICommandRepository;
import com.t1.alieva.tm.api.service.*;
import com.t1.alieva.tm.command.AbstractCommand;
import com.t1.alieva.tm.exception.AbstractException;
import com.t1.alieva.tm.exception.system.ArgumentNotSupportedException;
import com.t1.alieva.tm.exception.system.CommandNotSupportedException;
import com.t1.alieva.tm.repository.CommandRepository;
import com.t1.alieva.tm.service.*;
import com.t1.alieva.tm.util.SystemUtil;
import com.t1.alieva.tm.util.TerminalUtil;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;


public final class Bootstrap implements IServiceLocator {
    @NotNull
    private static final String PACKAGE_COMMANDS = "com.t1.alieva.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    @Getter
    private final ICommandService commandService = new CommandService(
            commandRepository);

    @NotNull
    @Getter
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    @Getter
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    @Getter
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstanse();

    @NotNull
    @Getter
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstanse();

    @NotNull
    @Getter
    private final ISystemEndpoint systemEndpoint = ISystemEndpoint.newInstanse();

    @NotNull
    @Getter
    private final IDomainEndpoint domainEndpoint = IDomainEndpoint.newInstanse();

    @NotNull
    @Getter
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstanse();

    @NotNull
    @Getter
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance();

    @NotNull
    @Getter
    private final ITokenService tokenService = new TokenService();

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            registry(clazz);
        }
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @Nullable final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void prepareStartup() {
        initPID();
        loggerService.info("**WELCOME TO TASK-MANAGER**");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutDown));
        fileScanner.start();
    }

    private void prepareShutDown() {

        loggerService.info("**TASK-MANAGER IS SHUTTING DOWN**");
        fileScanner.stop();
    }


    @SneakyThrows
    public void run(@Nullable final String[] args) {
        if (processArguments(args)) System.exit(0);

        prepareStartup();

        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("OK");
            } catch (final AbstractException e) {
                loggerService.error(e);
                System.out.println("FAIL");
            }
        }
    }

    @SneakyThrows
    private boolean processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        processArgument(args[0]);
        return true;
    }

    @SneakyThrows
    private void processArgument(@NotNull final String argument) {
        @Nullable final AbstractCommand abstractcommand = commandService.getCommandByArgument(argument);
        if (abstractcommand == null) throw new ArgumentNotSupportedException(argument);
        abstractcommand.execute();
    }


    public void processCommand(@Nullable final String command) throws
            AbstractException {
        processCommand(command, true);
    }

    @SneakyThrows
    public void processCommand(@NotNull final String command,
                               @NotNull boolean checkRoles) {
        @Nullable final AbstractCommand abstractcommand = commandService.getCommandByName(command);
        if (abstractcommand == null) throw new CommandNotSupportedException(command);
        abstractcommand.execute();
    }
}

