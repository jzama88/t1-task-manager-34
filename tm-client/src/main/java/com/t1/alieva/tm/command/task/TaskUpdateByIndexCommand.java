package com.t1.alieva.tm.command.task;

import com.t1.alieva.tm.dto.request.task.TaskUpdateByIndexRequest;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.system.AbstractSystemException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getName() {
        return "t-update-by-index";
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Update Task by Index.";
    }

    @Override
    public void execute() throws
            AbstractEntityNotFoundException,
            AbstractFieldException,
            AbstractSystemException,
            AbstractUserException {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(getToken());
        request.setIndex(index);
        request.setName(name);
        request.setDescription(description);
        getTaskEndpoint().updateTaskByIndex(request);
    }
}
