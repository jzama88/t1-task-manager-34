package com.t1.alieva.tm.command.project;

import com.t1.alieva.tm.dto.request.project.ProjectChangeStatusByIdRequest;
import com.t1.alieva.tm.enumerated.Status;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.system.AbstractSystemException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;

public final class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getName() {
        return "p-change-status-by-id";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Change Project Status by ID.";
    }

    @Override
    public void execute() throws
            AbstractEntityNotFoundException,
            AbstractSystemException,
            AbstractFieldException,
            AbstractUserException
            {
        System.out.println("[CHANGE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(getToken());
        request.setId(id);
        request.setStatus(status);
        getProjectEndpoint().changeProjectStatusById(request);
    }
}

