package com.t1.alieva.tm.command.project;

import com.t1.alieva.tm.dto.request.project.ProjectUpdateByIdRequest;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.system.AbstractSystemException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;


public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getName() {
        return "p-update-by-id";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Update Project by ID.";
    }

    @Override
    public void execute() throws
            AbstractEntityNotFoundException,
            AbstractSystemException,
            AbstractFieldException,
            AbstractUserException {
        System.out.println(("[UPDATE PROJECT BY ID]"));
        System.out.println(("[ENTER ID]"));
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println(("[ENTER NAME]"));
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println(("[ENTER DESCRIPTION]"));
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(getToken());
        request.setId(id);
        request.setName(name);
        request.setDescription(description);
        getProjectEndpoint().updateProjectById(request);

    }
}
