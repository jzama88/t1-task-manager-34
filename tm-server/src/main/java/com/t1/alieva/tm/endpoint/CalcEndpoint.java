package com.t1.alieva.tm.endpoint;

import com.t1.alieva.tm.api.endpoint.ICalcEndpoint;
import com.t1.alieva.tm.api.service.IServiceLocator;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "com.t1.alieva.tm.api.endpoint.ICalcEndpoint")
public final class CalcEndpoint extends AbstractEndpoint implements ICalcEndpoint {
    public CalcEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }
    @Override
    @WebMethod
    public int sum(
            @WebParam(name = "a") final int a,
            @WebParam(name = "b") final int b
    ) {
        return a + b;
    }

}
