package com.t1.alieva.tm.service;

import com.t1.alieva.tm.api.repository.IRepository;
import com.t1.alieva.tm.api.service.IService;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.entity.ModelNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.field.IdEmptyException;
import com.t1.alieva.tm.exception.field.IndexIncorrectException;
import com.t1.alieva.tm.model.AbstractModel;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {
    protected final R repository;

    public AbstractService(R repository) {
        this.repository = repository;
    }

    @Override
    @NotNull
    public M add(@Nullable M model) throws AbstractEntityNotFoundException {
        if (model == null) throw new ModelNotFoundException();
        return repository.add(model);
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        return repository.add(models);
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        return repository.set(models);
    }

    public void removeAll() {
        repository.removeAll();
    }

    @Override
    @NotNull
    public List<M> findAll(@Nullable Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    @Nullable
    public M findOneById(@Nullable String id) throws AbstractFieldException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);

    }

    @Override
    @Nullable
    public M findOneByIndex(@Nullable Integer index) throws
            AbstractFieldException {
        if (index == null || index < 0 || index >= repository.getSize()) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);

    }

    @Override
    @Nullable
    public M removeOne(@Nullable M model) throws AbstractEntityNotFoundException, AbstractFieldException {
        if (model == null) throw new ModelNotFoundException();
        return repository.removeOne(model);
    }

    @Override
    @Nullable
    public M removeOneById(@Nullable String id) throws AbstractFieldException, AbstractEntityNotFoundException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeOneById(id);
    }

    @Override
    @Nullable
    public M removeOneByIndex(@Nullable Integer index) throws AbstractFieldException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        return repository.removeOneByIndex(index);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    @Override
    @NotNull
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public boolean existsById(@Nullable String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    public void removeAll(@NotNull Collection<M> models) {
        repository.removeAll(models);
    }

}
