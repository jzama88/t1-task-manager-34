package com.t1.alieva.tm.endpoint;

import com.t1.alieva.tm.api.endpoint.IUserEndpoint;
import com.t1.alieva.tm.api.service.IAuthService;
import com.t1.alieva.tm.api.service.IServiceLocator;
import com.t1.alieva.tm.api.service.IUserService;
import com.t1.alieva.tm.dto.request.user.*;
import com.t1.alieva.tm.dto.response.user.*;
import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.model.Session;
import com.t1.alieva.tm.model.User;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import static com.t1.alieva.tm.api.endpoint.IEndpoint.REQUEST;

@WebService(endpointInterface = "com.t1.alieva.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @Override
    @NotNull
    @SneakyThrows
    @WebMethod
    public UserLockResponse lockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLockRequest request) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @NotNull final User user = getUserService().lockUserByLogin(login);
        return new UserLockResponse(user);
    }

    @Override
    @NotNull
    @SneakyThrows
    @WebMethod
    public UserUnlockResponse unlockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUnlockRequest request) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @NotNull final User user = getUserService().unlockUserByLogin(login);
        return new UserUnlockResponse(user);
    }

    @Override
    @NotNull
    @SneakyThrows
    @WebMethod
    public UserRemoveResponse removeUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRemoveRequest request) {
        check(request);
        @Nullable final String login = request.getLogin();
        @Nullable final User user = getUserService().removeByLogin(login);
        return new UserRemoveResponse(user);
    }

    @Override
    @NotNull
    @SneakyThrows
    @WebMethod
    public UserUpdateProfileResponse updateUserProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUpdateProfileRequest request) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @NotNull final String lastName = request.getLastName();
        @NotNull final String firstName = request.getFirstName();
        @NotNull final String middleName = request.getMiddleName();
        @Nullable final User user = getUserService().updateUser(userId, firstName, lastName, middleName);
        return new UserUpdateProfileResponse(user);
    }

    @Override
    @NotNull
    @SneakyThrows
    @WebMethod
    public UserChangePasswordResponse changeUserPassword(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserChangePasswordRequest request) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String password = request.getPassword();
        @Nullable final User user = getUserService().setPassword(userId, password);
        return new UserChangePasswordResponse(user);
    }

    @Override
    @NotNull
    @SneakyThrows
    @WebMethod
    public UserRegistryResponse registryUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRegistryRequest request) {
        check(request);
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @Nullable final String email = request.getEmail();
        @NotNull IAuthService authService = getServiceLocator().getAuthService();
        @Nullable final User user = authService.registry(login, password, email);
        return new UserRegistryResponse(user);
    }
}


