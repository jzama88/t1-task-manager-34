package com.t1.alieva.tm.api.repository;

import com.t1.alieva.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @NotNull
    Project create(
            @Nullable String userId,
            @NotNull String name,
            @NotNull String description);

    @NotNull
    Project create(
            @Nullable String userId,
            @NotNull String name);
}
