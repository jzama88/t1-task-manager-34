package com.t1.alieva.tm.component;

import com.t1.alieva.tm.api.endpoint.*;
import com.t1.alieva.tm.api.repository.IProjectRepository;
import com.t1.alieva.tm.api.repository.ISessionRepository;
import com.t1.alieva.tm.api.repository.ITaskRepository;
import com.t1.alieva.tm.api.repository.IUserRepository;
import com.t1.alieva.tm.api.service.*;
import com.t1.alieva.tm.endpoint.*;
import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.exception.AbstractException;
import com.t1.alieva.tm.model.User;
import com.t1.alieva.tm.repository.ProjectRepository;
import com.t1.alieva.tm.repository.SessionRepository;
import com.t1.alieva.tm.repository.TaskRepository;
import com.t1.alieva.tm.repository.UserRepository;
import com.t1.alieva.tm.service.*;
import com.t1.alieva.tm.util.SystemUtil;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;


public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "com.t1.alieva.tm.command";

    @NotNull
    @Getter
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    @Getter
    private final IProjectService projectService = new ProjectService(
            projectRepository);

    @NotNull
    @Getter
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    @Getter
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    @Getter
    private final IProjectTaskService projectTaskService = new ProjectTaskService(
            projectRepository,
            taskRepository
    );


    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    @Getter
    private final IUserService userService = new UserService(
            propertyService,
            userRepository,
            taskRepository,
            projectRepository
    );

    @NotNull
    @Getter
    private final ISessionService sessionService = new SessionService(sessionRepository);


    @NotNull
    @Getter
    private final IAuthService authService = new AuthService(
            userService,
            propertyService,
            sessionService
    );

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final ICalcEndpoint calcEndpoint = new CalcEndpoint(this);


    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);


    {
        registry(calcEndpoint);
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(domainEndpoint);
        registry(authEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
    }

    @SneakyThrows
    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = "localhost";
        @NotNull final String port = "8080";
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }


    private void initDemoData() throws
            AbstractException {

        @NotNull final User userTest = userService.create("test", "test");
        @NotNull final User userCustom = userService.create("user", "user", "user@user.ru");
        @NotNull final User userAdmin = userService.create("admin", "admin", Role.ADMIN);

        projectService.create(userTest.getId(), "PROJECT_TEST1", "Project 1 for TestUser");
        projectService.create(userTest.getId(), "PROJECT_TEST2", "Project 2 for TestUser");
        projectService.create(userCustom.getId(), "PROJECT_CUSTOM1", "Project for CustomUser");
        projectService.create(userAdmin.getId(), "PROJECT_ADMIN", "Project 1 for Admin");
        projectService.create(userAdmin.getId(), "PROJECT_ADMIN", "Project 2 for Admin");

        taskService.create(userTest.getId(), "TASK_TEST1", "test task 1");
        taskService.create(userTest.getId(), "TASK_TEST2", "test task 2");
        taskService.create(userCustom.getId(), "TASK_CUSTOM1", "test task 1");
        taskService.create(userCustom.getId(), "TASK_CUSTOM2", "test task 2");
        taskService.create(userAdmin.getId(), "TASK_ADMIN1", "test task 1");
        taskService.create(userAdmin.getId(), "TASK_ADMIN2", "test task 2");

    }

    public void start() throws
            AbstractException {
        initPID();
        initDemoData();
        loggerService.info("**WELCOME TO TASK-MANAGER**");
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
    }

    public void stop() {
        loggerService.info("**TASK-MANAGER IS SHUTTING DOWN**");
    }
}

