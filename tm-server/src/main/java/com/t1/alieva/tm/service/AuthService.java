package com.t1.alieva.tm.service;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.t1.alieva.tm.api.service.IAuthService;
import com.t1.alieva.tm.api.service.IPropertyService;
import com.t1.alieva.tm.api.service.ISessionService;
import com.t1.alieva.tm.api.service.IUserService;
import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.exception.AbstractException;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.entity.UserNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.field.LoginEmptyException;
import com.t1.alieva.tm.exception.field.PasswordEmptyException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.exception.user.AccessDeniedException;
import com.t1.alieva.tm.exception.user.AuthException;
import com.t1.alieva.tm.model.Session;
import com.t1.alieva.tm.model.User;
import com.t1.alieva.tm.util.CryptUtil;
import com.t1.alieva.tm.util.HashUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;


public class AuthService implements IAuthService {

    @NotNull
    private final IPropertyService propertyService;

    private final IUserService userService;

    private final ISessionService sessionService;


    public AuthService(@NotNull IUserService userService,
                       @NotNull IPropertyService propertyService,
                       @NotNull ISessionService sessionService) {
        this.propertyService = propertyService;
        this.userService = userService;
        this.sessionService = sessionService;
    }

    @Override
    public @Nullable User registry(@Nullable String login, @Nullable String password, @Nullable String email)
            throws AbstractEntityNotFoundException,
            AbstractUserException,
            AbstractFieldException {
        return userService.create(login, password, email);
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final User user) {
        return getToken(createSession(user));
    }

    @SneakyThrows
    private String getToken(@NotNull final Session session) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    @NotNull
    private Session createSession(@NotNull final User user) throws AbstractEntityNotFoundException {
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        @NotNull final Role role = user.getRole();
        session.setRole(role);
        return sessionService.add(session);
    }

    @NotNull
    public User check(@Nullable final String login, @Nullable final String password)
            throws AbstractFieldException,
            AuthException,
            AccessDeniedException,
            UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AuthException();
        if (user.getLocked()) throw new AuthException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash != null && !hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        return user;
    }

    @Override
    public @NotNull String login(
            @Nullable String login,
            @Nullable String password)
            throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AuthException();
        if (user.getLocked()) throw new AuthException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new AuthException();
        if (!hash.equals(user.getPasswordHash())) throw new AuthException();
        return getToken(user);
    }

    @Override
    @SneakyThrows
    public @NotNull Session validateToken(@Nullable String token) {
        if (token == null) throw new AccessDeniedException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull Session session = objectMapper.readValue(json, Session.class);

        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = session.getDate();
        final long delta = (currentDate.getTime() - sessionDate.getTime()) / 1000;
        @NotNull final int timeout = propertyService.getSessionTimeOut();
        if (delta > timeout) throw new AccessDeniedException();
        //if (!sessionService.existsById(session.getId())) throw new AccessDeniedException();

        return session;
    }

    @Override
    public void invalidate(@Nullable Session session)
            throws AbstractEntityNotFoundException, AbstractFieldException {
        if (session == null) return;
        sessionService.removeOne(session);
    }
}
