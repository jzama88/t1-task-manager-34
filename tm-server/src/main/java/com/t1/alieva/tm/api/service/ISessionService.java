package com.t1.alieva.tm.api.service;

import com.t1.alieva.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {
}
