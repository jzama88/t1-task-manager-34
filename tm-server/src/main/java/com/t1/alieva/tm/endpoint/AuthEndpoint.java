package com.t1.alieva.tm.endpoint;

import com.t1.alieva.tm.api.endpoint.IAuthEndpoint;
import com.t1.alieva.tm.api.service.IAuthService;
import com.t1.alieva.tm.api.service.IServiceLocator;
import com.t1.alieva.tm.dto.request.user.UserLoginRequest;
import com.t1.alieva.tm.dto.request.user.UserLogoutRequest;
import com.t1.alieva.tm.dto.request.user.UserProfileRequest;
import com.t1.alieva.tm.dto.response.user.UserLoginResponse;
import com.t1.alieva.tm.dto.response.user.UserLogoutResponse;
import com.t1.alieva.tm.dto.response.user.UserProfileResponse;
import com.t1.alieva.tm.exception.AbstractException;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AccessDeniedException;
import com.t1.alieva.tm.model.Session;
import com.t1.alieva.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import static com.t1.alieva.tm.api.endpoint.IEndpoint.REQUEST;


@WebService(endpointInterface = "com.t1.alieva.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {
    public AuthEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLoginResponse login(
            @WebParam(name = REQUEST,partName = REQUEST)
            @NotNull UserLoginRequest request) throws AbstractException {
        @NotNull final IAuthService authService = getServiceLocator().getAuthService();
        @NotNull final String token = authService.login(request.getLogin(),request.getPassword());
        return new UserLoginResponse(token);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST,partName = REQUEST)
            @NotNull UserLogoutRequest request) throws AbstractEntityNotFoundException, AccessDeniedException, AbstractFieldException {
       final Session session = check(request);
       getServiceLocator().getAuthService().invalidate(session);
       return new UserLogoutResponse();

    }

    @NotNull
    @Override
    @WebMethod
    public UserProfileResponse profile(
            @WebParam(name = REQUEST,partName = REQUEST)
            @NotNull UserProfileRequest request) throws AbstractFieldException, AccessDeniedException {
        final Session session = check(request);
        @Nullable final User user =  getServiceLocator().getUserService().findOneById(session.getUserId());
        return new UserProfileResponse(user);
    }
}
