package com.t1.alieva.tm.service;

import com.t1.alieva.tm.api.repository.ISessionRepository;
import com.t1.alieva.tm.api.service.ISessionService;
import com.t1.alieva.tm.model.Session;
import org.jetbrains.annotations.NotNull;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {
    public SessionService(@NotNull ISessionRepository repository) {
        super(repository);
    }
}
