package com.t1.alieva.tm.api.service;

import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IProjectTaskService {

    @Nullable Task bindTaskToProject(
            @NotNull String userId,
            @Nullable String projectId,
            @Nullable String taskId) throws
            AbstractFieldException,
            AbstractEntityNotFoundException;

    void removeProjectById(
            @NotNull String userId,
            @Nullable String projectId) throws
            AbstractFieldException,
            AbstractEntityNotFoundException;

    @Nullable Task unbindTaskFromProject(
            @NotNull String userId,
            @Nullable String projectId,
            @Nullable String taskId) throws
            AbstractFieldException,
            AbstractEntityNotFoundException;
}
