package com.t1.alieva.tm.service;

import com.t1.alieva.tm.api.repository.IProjectRepository;
import com.t1.alieva.tm.api.repository.ITaskRepository;
import com.t1.alieva.tm.api.repository.IUserRepository;
import com.t1.alieva.tm.api.service.IPropertyService;
import com.t1.alieva.tm.api.service.IUserService;
import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.entity.UserNotFoundException;
import com.t1.alieva.tm.exception.field.*;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.exception.user.ExistsEmailException;
import com.t1.alieva.tm.exception.user.ExistsLoginException;
import com.t1.alieva.tm.exception.user.RoleEmptyException;
import com.t1.alieva.tm.model.User;
import com.t1.alieva.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    public UserService(
            @NotNull final IPropertyService propertyService,
            @NotNull final IUserRepository userRepository,
            @NotNull final ITaskRepository taskRepository,
            @NotNull final IProjectRepository projectRepository) {

        super(userRepository);
        this.propertyService = propertyService;
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password) throws
            AbstractFieldException,
            AbstractUserException,
            AbstractEntityNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        return repository.add(user);
    }

    @Override
    @NotNull
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @NotNull final String email) throws
            AbstractFieldException,
            AbstractUserException,
            AbstractEntityNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException();
        @NotNull final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    @NotNull
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role) throws
            AbstractFieldException,
            AbstractUserException,
            AbstractEntityNotFoundException {

        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull final User user = create(login, password);
        if (role != null) user.setRole(role);
        return user;
    }

    @Override
    @NotNull
    public User findByLogin(@Nullable final String login) throws AbstractFieldException, UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = repository.findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    @Nullable
    public User findByEmail(@Nullable final String email) throws AbstractFieldException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final User user = repository.findOneByEmail(email);
        if (user == null) return null;
        return user;
    }

    @Override
    @Nullable
    public User removeOne(@Nullable final User model) throws AbstractEntityNotFoundException, AbstractFieldException {
        if (model == null) return null;
        @Nullable final User user = super.removeOne(model);
        if (user == null) return null;
        @NotNull final String userId = user.getId();
        taskRepository.removeAll(userId);
        projectRepository.removeAll(userId);
        return user;
    }

    @Override
    @NotNull
    public User removeByLogin(@Nullable final String login) throws AbstractFieldException, AbstractEntityNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        return removeOne(user);
    }

    @Override
    @NotNull
    public User removeByEmail(@Nullable final String email) throws AbstractEntityNotFoundException, AbstractFieldException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final User user = findByEmail(email);
        return removeOne(user);
    }

    @Override
    @NotNull
    public User setPassword(
            @Nullable final String id,
            @Nullable final String password) throws
            AbstractFieldException,
            AbstractEntityNotFoundException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return user;
    }

    @Override
    @NotNull
    public User updateUser(
            @Nullable final String id,
            @NotNull final String firstName,
            @NotNull final String lastName,
            @NotNull final String middleName) throws
            AbstractFieldException,
            AbstractEntityNotFoundException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return repository.isLoginExist(login);
    }

    @Override
    public boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return repository.isEmailExist(email);
    }

    @Override
    public @NotNull User lockUserByLogin(@Nullable final String login) throws
            AbstractFieldException,
            UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final User user = findByLogin(login);
        user.setLocked(true);
        return user;
    }

    @Override
    public @NotNull User unlockUserByLogin(@Nullable final String login) throws
            AbstractFieldException,
            UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final User user = findByLogin(login);
        user.setLocked(false);
        return user;
    }

}
