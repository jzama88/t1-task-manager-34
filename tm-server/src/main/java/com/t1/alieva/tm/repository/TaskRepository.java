package com.t1.alieva.tm.repository;

import com.t1.alieva.tm.api.repository.ITaskRepository;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    @NotNull
    public Task create(
            @Nullable final String userId,
            @NotNull final String name) throws
            AbstractEntityNotFoundException {
        @NotNull final Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        return add(task);
    }

    @Override
    @NotNull
    public Task create(
            @Nullable final String userId,
            @NotNull final String name,
            @NotNull final String description) throws
            AbstractEntityNotFoundException {
        @NotNull final Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        return add(task);
    }

    @Override
    @NotNull
    public List<Task> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId) {
        return models.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .filter(m -> projectId.equals(m.getProjectId()))
                .collect(Collectors.toList());
    }
}
