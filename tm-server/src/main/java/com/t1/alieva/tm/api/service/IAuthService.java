package com.t1.alieva.tm.api.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.t1.alieva.tm.exception.AbstractException;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.model.Session;
import com.t1.alieva.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.naming.AuthenticationException;

public interface IAuthService {

    @Nullable
    User registry(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email) throws
            AbstractUserException,
            AbstractFieldException,
            AbstractEntityNotFoundException;

    //@NotNull
   // User check(@Nullable String login, @Nullable String password);

    @NotNull
    String login(@Nullable String login,@Nullable String password) throws AbstractException;

    @NotNull
    Session validateToken (@Nullable String token) ;

    void invalidate(@Nullable final Session session) throws AbstractEntityNotFoundException, AbstractFieldException;

}
