package com.t1.alieva.tm.api.repository;


import com.t1.alieva.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
