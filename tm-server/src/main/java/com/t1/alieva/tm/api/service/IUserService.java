package com.t1.alieva.tm.api.service;

import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.entity.UserNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.model.User;
import com.t1.alieva.tm.api.repository.IRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IUserService extends IRepository<User> {

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password) throws
            AbstractFieldException,
            AbstractUserException,
            AbstractEntityNotFoundException;

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password,
            @NotNull String email) throws
            AbstractFieldException,
            AbstractUserException,
            AbstractEntityNotFoundException;

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable Role role) throws
            AbstractFieldException,
            AbstractUserException,
            AbstractEntityNotFoundException;

    @NotNull
    User add(@Nullable User user) throws AbstractEntityNotFoundException;

    @NotNull
    User findByLogin(@Nullable String login) throws
            AbstractFieldException,
            UserNotFoundException;

    User findByEmail(@Nullable String email) throws
            AbstractFieldException;

    @NotNull
    User removeByLogin(@Nullable String login) throws
            AbstractFieldException,
            AbstractEntityNotFoundException;

    @NotNull
    User removeByEmail(@Nullable String email) throws
            AbstractEntityNotFoundException,
            AbstractFieldException;

    @NotNull
    User setPassword(
            @Nullable String id,
            @Nullable String password) throws
            AbstractFieldException,
            AbstractEntityNotFoundException;

    @NotNull
    User updateUser(
            @Nullable String id,
            @NotNull String firstName,
            @NotNull String lastName,
            @NotNull String middleName) throws
            AbstractFieldException,
            AbstractEntityNotFoundException;

    boolean isLoginExist(@Nullable String login);

    boolean isEmailExist(@Nullable String email);

    @NotNull User lockUserByLogin(@Nullable String login) throws
            AbstractFieldException,
            UserNotFoundException;

    @NotNull User unlockUserByLogin(@Nullable String login) throws
            AbstractFieldException,
            UserNotFoundException;
}
