package com.t1.alieva.tm;


import com.t1.alieva.tm.component.Bootstrap;
import com.t1.alieva.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


public final class Application {

    public static void main(@Nullable String[] args) throws
            AbstractException {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }
}
