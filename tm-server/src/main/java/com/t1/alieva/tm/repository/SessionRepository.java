package com.t1.alieva.tm.repository;

import com.t1.alieva.tm.api.repository.ISessionRepository;
import com.t1.alieva.tm.model.Session;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {
}
