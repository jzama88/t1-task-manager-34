package com.t1.alieva.tm.dto.response.project;

import com.t1.alieva.tm.model.Project;
import org.jetbrains.annotations.Nullable;

public final class ProjectUpdateByIdResponse extends AbstractProjectResponse {
    public ProjectUpdateByIdResponse(@Nullable Project project) {
        super(project);
    }
}
