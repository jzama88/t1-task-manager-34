package com.t1.alieva.tm.dto.request.task;

import com.t1.alieva.tm.dto.request.user.AbstractUserRequest;
import org.jetbrains.annotations.Nullable;

public final class TaskClearRequest extends AbstractUserRequest
{
    public TaskClearRequest(@Nullable final String token) {
        super(token);
    }
}
