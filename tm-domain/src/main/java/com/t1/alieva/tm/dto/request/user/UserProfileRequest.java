package com.t1.alieva.tm.dto.request.user;


import org.jetbrains.annotations.Nullable;

public final class UserProfileRequest extends AbstractUserRequest
{
    public UserProfileRequest(@Nullable final String token) {
        super(token);
    }
}
