package com.t1.alieva.tm.dto.response.user;

import com.t1.alieva.tm.dto.response.user.AbstractUserResponse;
import com.t1.alieva.tm.model.User;
import org.jetbrains.annotations.Nullable;

public final class UserLockResponse extends AbstractUserResponse {
    public UserLockResponse(@Nullable final User user) {
        super(user);
    }

    public UserLockResponse(@Nullable final String token) {
        super(token);
    }

}
