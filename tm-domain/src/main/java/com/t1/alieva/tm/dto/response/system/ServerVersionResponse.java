package com.t1.alieva.tm.dto.response.system;

import com.t1.alieva.tm.dto.response.AbstractResponse;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class ServerVersionResponse extends AbstractResponse {

    private String version;
}
