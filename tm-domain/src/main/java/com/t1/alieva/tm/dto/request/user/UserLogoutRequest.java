package com.t1.alieva.tm.dto.request.user;


import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class UserLogoutRequest extends AbstractUserRequest {

    public UserLogoutRequest(@Nullable String token) {
        super(token);
    }

}
