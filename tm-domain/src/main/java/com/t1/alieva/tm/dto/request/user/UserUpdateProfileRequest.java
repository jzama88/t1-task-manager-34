package com.t1.alieva.tm.dto.request.user;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class UserUpdateProfileRequest extends AbstractUserRequest {

    @Nullable
    private String lastName;

    @Nullable
    private String firstName;

    @Nullable
    private String middleName;

    public UserUpdateProfileRequest(@Nullable final String token) {
        super(token);
    }

}
