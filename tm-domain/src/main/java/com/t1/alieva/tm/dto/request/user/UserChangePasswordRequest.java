package com.t1.alieva.tm.dto.request.user;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class UserChangePasswordRequest extends AbstractUserRequest {
    @Nullable
    private String password;

    public UserChangePasswordRequest(@Nullable final String token) {
        super(token);
    }

}
