package com.t1.alieva.tm.dto.request.task;

import com.t1.alieva.tm.dto.request.user.AbstractUserRequest;
import com.t1.alieva.tm.enumerated.Status;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class TaskChangeStatusByIdRequest extends AbstractUserRequest {
    @Nullable
    private String id;

    @Nullable
    private Status status;


    public TaskChangeStatusByIdRequest(@Nullable final String token) {
        super(token);
    }

}
