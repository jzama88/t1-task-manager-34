package com.t1.alieva.tm.dto.request.data;

import com.t1.alieva.tm.dto.request.user.AbstractUserRequest;
import org.jetbrains.annotations.Nullable;

public final class DataBackupLoadRequest extends AbstractUserRequest
{

    public DataBackupLoadRequest(@Nullable final String token) {
        super(token);
    }
}
