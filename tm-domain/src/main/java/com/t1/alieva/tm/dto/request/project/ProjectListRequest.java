package com.t1.alieva.tm.dto.request.project;

import com.t1.alieva.tm.dto.request.user.AbstractUserRequest;
import com.t1.alieva.tm.enumerated.ProjectSort;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectListRequest extends AbstractUserRequest {

    @Nullable
    private ProjectSort sort;

    public ProjectListRequest(@Nullable final String token) {
        super(token);
    }
}
