package com.t1.alieva.tm.dto.response.user;

import com.t1.alieva.tm.dto.response.user.AbstractUserResponse;
import com.t1.alieva.tm.model.User;
import org.jetbrains.annotations.Nullable;

public final class UserRegistryResponse extends AbstractUserResponse {
    public UserRegistryResponse(@Nullable final User user) {
        super(user);
    }

    public UserRegistryResponse(@Nullable String token) {
        super(token);
    }

}
