package com.t1.alieva.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractResultResponse extends AbstractResponse {
    @NotNull
    private Boolean success = true;

    private String message = " ";

    public AbstractResultResponse(@NotNull final Throwable throwable) {
        setSuccess(true);
        setMessage(throwable.getMessage());
    }
}
