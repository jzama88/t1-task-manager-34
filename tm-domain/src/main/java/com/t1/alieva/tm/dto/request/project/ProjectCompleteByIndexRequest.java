package com.t1.alieva.tm.dto.request.project;

import com.t1.alieva.tm.dto.request.user.AbstractUserRequest;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class ProjectCompleteByIndexRequest extends AbstractUserRequest
{
    @Nullable
    private Integer index;

    public ProjectCompleteByIndexRequest(@Nullable final String token) {
        super(token);
    }
}
