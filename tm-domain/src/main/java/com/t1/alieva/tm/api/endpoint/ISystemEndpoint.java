package com.t1.alieva.tm.api.endpoint;

import com.t1.alieva.tm.dto.request.system.ServerAboutRequest;
import com.t1.alieva.tm.dto.request.system.ServerVersionRequest;
import com.t1.alieva.tm.dto.response.system.ServerAboutResponse;
import com.t1.alieva.tm.dto.response.system.ServerVersionResponse;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ISystemEndpoint extends IEndpoint {

    @NotNull
    String NAME = "SystemEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static ISystemEndpoint newInstanse() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ISystemEndpoint newInstanse(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, ISystemEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, ISystemEndpoint.class);
    }

    @NotNull
    @WebMethod
    ServerAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ServerAboutRequest request
    );

    @NotNull
    @WebMethod
    ServerVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ServerVersionRequest request
    );

    @WebMethod(exclude = true)
    static void main(String[] args) {
        System.out.println(ISystemEndpoint.newInstanse().getAbout(new ServerAboutRequest()));
    }
}
