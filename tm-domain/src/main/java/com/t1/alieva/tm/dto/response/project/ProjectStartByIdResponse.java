package com.t1.alieva.tm.dto.response.project;

import com.t1.alieva.tm.model.Project;
import org.jetbrains.annotations.Nullable;


public class ProjectStartByIdResponse extends AbstractProjectResponse
{
    public ProjectStartByIdResponse(@Nullable Project project) {
        super(project);
    }
}
