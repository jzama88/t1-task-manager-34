package com.t1.alieva.tm.dto.response.task;

import com.t1.alieva.tm.model.Task;
import org.jetbrains.annotations.Nullable;

public final class TaskCompleteByIdResponse extends AbstractTaskResponse {
    public TaskCompleteByIdResponse(@Nullable Task task) {
        super(task);
    }
}
