package com.t1.alieva.tm.dto.response.project;

import com.t1.alieva.tm.dto.response.project.AbstractProjectResponse;
import com.t1.alieva.tm.model.Project;
import org.jetbrains.annotations.Nullable;

public final class ProjectChangeStatusByIndexResponse extends AbstractProjectResponse {
    public ProjectChangeStatusByIndexResponse(@Nullable Project project) {
        super(project);
    }
}


