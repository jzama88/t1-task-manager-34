package com.t1.alieva.tm.dto.response.project;

import com.t1.alieva.tm.model.Project;
import org.jetbrains.annotations.Nullable;

public class ProjectStartByIndexResponse extends AbstractProjectResponse{
    public ProjectStartByIndexResponse(@Nullable Project project) {
        super(project);
    }
}
