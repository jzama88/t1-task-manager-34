package com.t1.alieva.tm.dto.request.project;

import com.t1.alieva.tm.dto.request.user.AbstractUserRequest;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class ProjectStartByIndexRequest extends AbstractUserRequest {
    @Nullable
    private Integer index;

    public ProjectStartByIndexRequest(@Nullable final String token) {
        super(token);
    }
}
