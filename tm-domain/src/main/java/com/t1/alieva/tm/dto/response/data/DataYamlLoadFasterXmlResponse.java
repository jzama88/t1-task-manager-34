package com.t1.alieva.tm.dto.response.data;


import com.t1.alieva.tm.dto.response.AbstractResponse;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class DataYamlLoadFasterXmlResponse extends AbstractResponse {
}
