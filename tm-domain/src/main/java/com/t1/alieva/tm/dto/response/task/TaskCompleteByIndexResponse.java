package com.t1.alieva.tm.dto.response.task;

import com.t1.alieva.tm.model.Task;
import org.jetbrains.annotations.Nullable;

public class TaskCompleteByIndexResponse extends AbstractTaskResponse{
    public TaskCompleteByIndexResponse(@Nullable Task task) {
        super(task);
    }
}
