package com.t1.alieva.tm.dto.request.user;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class UserUnlockRequest extends AbstractUserRequest
{
    @Nullable
    private String login;

    public UserUnlockRequest(@Nullable final String token) {
        super(token);
    }

}
