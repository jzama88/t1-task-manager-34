package com.t1.alieva.tm.exception.user;

public final class AccessDeniedException extends AbstractUserException {

    public AccessDeniedException() {
        super("Error! You're not logged in.Please log in and try again...");
    }
}
