package com.t1.alieva.tm.model;

import com.t1.alieva.tm.enumerated.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Session extends AbstractUserOwnedModel {
    @NotNull
    private Date date = new Date();

    @Nullable
    private Role role = null;
}
