package com.t1.alieva.tm.exception.user;

public final class AuthException extends AbstractUserException
{
    public AuthException() {
        super("Error! Auth Exception..");
    }
}

