package com.t1.alieva.tm.dto.request.data;

import com.t1.alieva.tm.dto.request.user.AbstractUserRequest;
import org.jetbrains.annotations.Nullable;

public final class DataBase64LoadRequest extends AbstractUserRequest
{

    public DataBase64LoadRequest(@Nullable final String token) {
        super(token);
    }

}
