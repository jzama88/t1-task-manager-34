package com.t1.alieva.tm.dto.response.user;

import com.t1.alieva.tm.dto.response.user.AbstractUserResponse;
import com.t1.alieva.tm.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class UserChangePasswordResponse extends AbstractUserResponse {

    public UserChangePasswordResponse(@Nullable final User user) {
        super(user);
    }

    public UserChangePasswordResponse(@Nullable final String token) {
        super(token);
    }

}
