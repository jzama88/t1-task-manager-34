package com.t1.alieva.tm.dto.request.data;

import com.t1.alieva.tm.dto.request.user.AbstractUserRequest;
import org.jetbrains.annotations.Nullable;

public final class DataJsonLoadFasterXmlRequest extends AbstractUserRequest
{
    public DataJsonLoadFasterXmlRequest(@Nullable final String token) {
        super(token);
    }
}
