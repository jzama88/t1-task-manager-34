package com.t1.alieva.tm.dto.request.data;

import com.t1.alieva.tm.dto.request.user.AbstractUserRequest;
import org.jetbrains.annotations.Nullable;

public final class DataXmlLoadJaxBRequest extends AbstractUserRequest
{
    public DataXmlLoadJaxBRequest(@Nullable final String token) {
        super(token);
    }
}
