package com.t1.alieva.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ICalcEndpoint extends IEndpoint{

    String NAME = "CalcEndpoint";

    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static ICalcEndpoint newInstanse() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ICalcEndpoint newInstanse(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, ICalcEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ICalcEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, ICalcEndpoint.class);
    }

    @WebMethod
    int sum(
            @WebParam(name = "a") int a,
            @WebParam(name = "b") int b);

    @WebMethod(exclude = true)
    static void main(String[] args) {
        System.out.println(ICalcEndpoint.newInstanse().sum(5, 7));
    }
}
