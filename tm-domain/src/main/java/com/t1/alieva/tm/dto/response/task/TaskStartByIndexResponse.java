package com.t1.alieva.tm.dto.response.task;

import com.t1.alieva.tm.model.Task;
import org.jetbrains.annotations.Nullable;

public class TaskStartByIndexResponse extends AbstractTaskResponse{
    public TaskStartByIndexResponse(@Nullable Task task) {
        super(task);
    }
}
