package com.t1.alieva.tm.dto.response.user;

import com.t1.alieva.tm.dto.response.AbstractResultResponse;
import com.t1.alieva.tm.model.User;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Setter
@NoArgsConstructor
public final class UserLoginResponse extends AbstractUserResponse {


    public UserLoginResponse(@NotNull Throwable throwable) {
        super(throwable);
    }


    public UserLoginResponse(@Nullable final String token) {
        super(token);
    }

}
