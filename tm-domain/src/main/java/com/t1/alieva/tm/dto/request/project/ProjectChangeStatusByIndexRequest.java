package com.t1.alieva.tm.dto.request.project;

import com.t1.alieva.tm.dto.request.user.AbstractUserRequest;
import com.t1.alieva.tm.enumerated.Status;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class ProjectChangeStatusByIndexRequest extends AbstractUserRequest {
    @Nullable
    private Integer index;

    @Nullable
    private Status status;


    public ProjectChangeStatusByIndexRequest(@Nullable final String token) {
        super(token);
    }

}
