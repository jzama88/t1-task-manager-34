package com.t1.alieva.tm.dto.response.project;

import com.t1.alieva.tm.model.Project;
import org.jetbrains.annotations.Nullable;

public final class ProjectRemoveByIdResponse extends AbstractProjectResponse {
    public ProjectRemoveByIdResponse(@Nullable Project project) {
        super(project);
    }
}
