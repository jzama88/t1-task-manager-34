package com.t1.alieva.tm.dto.request.project;

import com.t1.alieva.tm.dto.request.user.AbstractUserRequest;
import com.t1.alieva.tm.enumerated.Status;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class ProjectChangeStatusByIdRequest extends AbstractUserRequest {
    @Nullable
    private String id;

    @Nullable
    private Status status;

    private String userId;

    public ProjectChangeStatusByIdRequest(@Nullable final String token) {
        super(token);
    }
}
