package com.t1.alieva.tm.dto.response.task;

import com.t1.alieva.tm.model.Task;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class TaskUpdateByIndexResponse extends AbstractTaskResponse {
    public TaskUpdateByIndexResponse(@Nullable Task task) {
        super(task);
    }
}
