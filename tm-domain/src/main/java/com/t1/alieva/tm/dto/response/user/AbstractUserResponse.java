package com.t1.alieva.tm.dto.response.user;

import com.t1.alieva.tm.dto.response.AbstractResponse;
import com.t1.alieva.tm.dto.response.AbstractResultResponse;
import com.t1.alieva.tm.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractUserResponse extends AbstractResultResponse {
    @Nullable
    private User user;

    public AbstractUserResponse(@Nullable User user) {
        this.user = user;
    }

    @Getter
    @Nullable
    private String token;

    public AbstractUserResponse(@Nullable final String token) {
        this.token = token;
    }

    public AbstractUserResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
