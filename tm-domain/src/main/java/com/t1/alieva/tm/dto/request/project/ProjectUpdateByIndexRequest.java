package com.t1.alieva.tm.dto.request.project;

import com.t1.alieva.tm.dto.request.user.AbstractUserRequest;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class ProjectUpdateByIndexRequest extends AbstractUserRequest {
    @Nullable
    private Integer index;

    @Nullable
    private String name;

    @Nullable
    private String description;


    public ProjectUpdateByIndexRequest(@Nullable final String token) {
        super(token);
    }

}
