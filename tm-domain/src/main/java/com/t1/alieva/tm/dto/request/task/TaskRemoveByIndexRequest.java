package com.t1.alieva.tm.dto.request.task;

import com.t1.alieva.tm.dto.request.user.AbstractUserRequest;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class TaskRemoveByIndexRequest extends AbstractUserRequest {
    @Nullable
    private Integer index;

    public TaskRemoveByIndexRequest(@Nullable final String token) {
        super(token);
    }

}
