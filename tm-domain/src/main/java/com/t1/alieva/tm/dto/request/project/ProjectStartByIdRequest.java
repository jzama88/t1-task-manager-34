package com.t1.alieva.tm.dto.request.project;

import com.t1.alieva.tm.dto.request.user.AbstractUserRequest;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class ProjectStartByIdRequest extends AbstractUserRequest
{
    @Nullable
    private String id;

    public ProjectStartByIdRequest(@Nullable final String token) {
        super(token);
    }
}
