package com.t1.alieva.tm.dto.request.task;

import com.t1.alieva.tm.dto.request.user.AbstractUserRequest;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class TaskListByProjectIdRequest extends AbstractUserRequest {
    @Nullable
    private String projectId;

    public TaskListByProjectIdRequest(@Nullable final String token) {
        super(token);
    }

}
