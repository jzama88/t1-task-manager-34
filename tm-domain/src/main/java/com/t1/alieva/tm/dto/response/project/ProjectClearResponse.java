package com.t1.alieva.tm.dto.response.project;

import com.t1.alieva.tm.dto.response.AbstractResponse;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class ProjectClearResponse extends AbstractResponse {
}

