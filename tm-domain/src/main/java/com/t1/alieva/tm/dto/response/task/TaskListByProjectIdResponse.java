package com.t1.alieva.tm.dto.response.task;

import com.t1.alieva.tm.dto.response.AbstractResponse;
import com.t1.alieva.tm.model.Task;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListByProjectIdResponse extends AbstractResponse {
    @Nullable
    private List<Task> tasks;

    public TaskListByProjectIdResponse(@Nullable List<Task> tasks) {
        this.tasks = tasks;
    }
}
